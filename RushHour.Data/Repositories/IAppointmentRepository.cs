﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RushHour.Entities;

namespace RushHour.Data.Repositories
{
    public interface IAppointmentRepository : IRepository<Appointment>
    {
        IQueryable<Appointment> GetAppointments();
    }
}