﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RushHour.Entities;

namespace RushHour.Data.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected RushHourContext db;
        protected DbSet<T> dbSet;

        public BaseRepository(RushHourContext db)
        {
            this.db = db;
            dbSet = db.Set<T>();
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            db.SaveChanges();
        }

        public T Get(Guid Id)
        {
            return dbSet.Find(Id);
        }

        public IQueryable<T> GetAll()
        {
            return dbSet;
        }

        public IQueryable<Appointment> GetAllAppointments()
        {
            var result = db.Appointments.Include(a => a.Activity);

            return result;
        }

        public void Insert(T entity)
        {
            dbSet.Add(entity);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}