﻿using Microsoft.EntityFrameworkCore;
using RushHour.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RushHour.Data.Repositories
{
    public class AppointmentRepository : BaseRepository<Appointment>, IAppointmentRepository
    {
        public AppointmentRepository(RushHourContext db) : base(db)
        {
        }

        public IQueryable<Appointment> GetAppointments()
        {
            var result = db.Appointments.Include(a => a.Activity).Where(a => Contex.currentUser.Id == a.UserId);

            return result;
        }
    }
}