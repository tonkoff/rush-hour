﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RushHour.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activities_Appointments_AppointmentId",
                table: "Activities");

            migrationBuilder.DropIndex(
                name: "IX_Activities_AppointmentId",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "AppointmentId",
                table: "Activities");

            migrationBuilder.AddColumn<Guid>(
                name: "ActivityId",
                table: "Appointments",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_ActivityId",
                table: "Appointments",
                column: "ActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointments_Activities_ActivityId",
                table: "Appointments",
                column: "ActivityId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointments_Activities_ActivityId",
                table: "Appointments");

            migrationBuilder.DropIndex(
                name: "IX_Appointments_ActivityId",
                table: "Appointments");

            migrationBuilder.DropColumn(
                name: "ActivityId",
                table: "Appointments");

            migrationBuilder.AddColumn<Guid>(
                name: "AppointmentId",
                table: "Activities",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Activities_AppointmentId",
                table: "Activities",
                column: "AppointmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Activities_Appointments_AppointmentId",
                table: "Activities",
                column: "AppointmentId",
                principalTable: "Appointments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
