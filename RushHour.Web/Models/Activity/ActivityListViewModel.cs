﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.Activity
{
    public class ActivityListViewModel
    {
        public List<RushHour.Entities.Activity> Activities { get; set; }
    }
}