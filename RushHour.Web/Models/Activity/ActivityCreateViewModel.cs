﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.Activity
{
    public class ActivityCreateViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public float Duration { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
    }
}