﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.Appointment
{
    public class AppointmentEditViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDateTime { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDateTime { get; set; }

        public Guid UserId { get; set; }
        public Guid ActivityId { get; set; }

        public List<RushHour.Entities.Activity> Activity { get; set; }
    }
}