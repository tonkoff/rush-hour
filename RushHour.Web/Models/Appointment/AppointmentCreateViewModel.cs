﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.Appointment
{
    public class AppointmentCreateViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDateTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndDateTime { get; set; }

        public Guid UserId { get; set; }
        public Guid ActivityId { get; set; }

        public virtual List<RushHour.Entities.Activity> Activity { get; set; }
    }
}