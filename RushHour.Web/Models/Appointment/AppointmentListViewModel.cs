﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.Appointment
{
    public class AppointmentListViewModel
    {
        public List<RushHour.Entities.Appointment> Appointments { get; set; }
    }
}