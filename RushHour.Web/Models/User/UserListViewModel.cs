﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.User
{
    public class UserListViewModel
    {
        public List<RushHour.Entities.User> Users { get; set; }
    }
}