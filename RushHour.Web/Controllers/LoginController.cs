﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RushHour.Services;
using RushHour.Entities;
using RushHour.Web.Models.Login;
using AutoMapper;
using Microsoft.AspNetCore.Http;

namespace RushHour.Web.Controllers
{
    public class LoginController : Controller
    {
        private IDataService<User> service;

        public LoginController(IDataService<User> service)
        {
            this.service = service;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(LoginViewModel model)
        {
            var item = Mapper.Map<LoginViewModel, User>(model);

            service.Insert(item);

            Contex.currentUser = item;

            return RedirectToAction("Index", "User");
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(LoginViewModel model)
        {
            List<User> users = service.GetAll();

            foreach (var user in users)
            {
                if (user.Email == model.Email && user.Password == model.Password)
                {
                    Contex.currentUser = user;
                    break;
                }
            }

            if (Contex.currentUser.Email == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
    }
}