﻿using Microsoft.AspNetCore.Mvc;
using System;
using RushHour.Entities;
using RushHour.Services;
using RushHour.Web.Models.User;
using AutoMapper;
using System.Collections.Generic;
using RushHour.Web;

namespace RushHour.Web.Controllers
{
    public class UserController : Controller
    {
        private IDataService<User> service;

        public UserController(IDataService<User> service)
        {
            this.service = service;
        }

        public IActionResult Index()
        {
            //  var items = AutoMapper.Mapper.Map<IEnumerable<User>, IEnumerable<UserListViewModel>>(service.GetAll());

            UserListViewModel model = new UserListViewModel()
            {
                Users = service.GetAll()
            };

            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(UserCreateViewModel model)
        {
            //var item = Mapper.Map<UserCreateViewModel, User>(model);
            //  var item = Mapper.Map<User, UserViewModel>(model);

            if (ModelState.IsValid)
            {
                User user = new User();

                user.Email = model.Email;
                user.Password = model.Password;
                user.Name = model.Name;
                user.Phone = model.Phone;

                service.Insert(user);

                return RedirectToAction("Index", "User");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            User user = service.Get(id);

            service.Delete(user);

            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var items = AutoMapper.Mapper.Map<User, UserEditViewModel>(service.Get(id));

            return View(items);
        }

        [HttpPost]
        public IActionResult Edit(UserEditViewModel model)
        {
            //var item = Mapper.Map<UserEditViewModel, User>(model);
            if (ModelState.IsValid)
            {
                User user = new User();

                user.Id = model.Id;
                user.Email = model.Email;
                user.Password = model.Password;
                user.Name = model.Name;
                user.Phone = model.Phone;

                service.Update(user);

                return RedirectToAction("Index", "User");
            }

            return View(model);
        }
    }
}