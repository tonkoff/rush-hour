﻿using Microsoft.AspNetCore.Mvc;
using System;
using RushHour.Entities;
using RushHour.Services;
using RushHour.Web.Models.Activity;
using AutoMapper;
using System.Collections.Generic;

namespace RushHour.Web.Controllers
{
    public class ActivityController : Controller
    {
        private IDataService<Activity> service;

        public ActivityController(IDataService<Activity> service)
        {
            this.service = service;
        }

        public IActionResult Index()
        {
            // var items = AutoMapper.Mapper.Map<IEnumerable<Activity>, IEnumerable<ActivityViewModel>>(service.GetAll());

            ActivityListViewModel model = new ActivityListViewModel
            {
                Activities = service.GetAll()
            };

            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ActivityCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Activity activity = new Activity();

                activity.Name = model.Name;
                activity.Duration = model.Duration;
                activity.Price = model.Price;

                service.Insert(activity);

                return RedirectToAction("Index", "Activity");
            }

            return View(model);

            // var item = Mapper.Map<ActivityViewModel, Activity>(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Activity activity = service.Get(id);

            service.Delete(activity);

            return RedirectToAction("Index", "Activity");
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var items = AutoMapper.Mapper.Map<Activity, ActivityEditViewModel>(service.Get(id));

            return View(items);
        }

        [HttpPost]
        public IActionResult Edit(ActivityEditViewModel model)
        {
            // var item = Mapper.Map<ActivityViewModel, Activity>(model);

            if (ModelState.IsValid)
            {
                Activity activity = new Activity();

                activity.Id = model.Id;
                activity.Name = model.Name;
                activity.Duration = model.Duration;
                activity.Price = model.Price;

                service.Update(activity);

                return RedirectToAction("Index", "Activity");
            }

            return View(model);
        }
    }
}