﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RushHour.Services;
using RushHour.Entities;
using RushHour.Web.Models.Appointment;
using AutoMapper;
using RushHour.Data;
using Microsoft.EntityFrameworkCore;

namespace RushHour.Web.Controllers
{
    public class AppointmentController : Controller
    {
        private IAppointmentService service;

        private IDataService<Activity> activityService;

        public AppointmentController(IAppointmentService service, IDataService<Activity> activityService)
        {
            this.service = service;
            this.activityService = activityService;
        }

        public IActionResult Index()
        {
            //  var items = AutoMapper.Mapper.Map<IEnumerable<Appointment>, IEnumerable<AppointmentViewModel>>(service.GetAllAppointments());

            AppointmentListViewModel model = new AppointmentListViewModel
            {
                Appointments = service.GetAppointments()
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            AppointmentCreateViewModel model = new AppointmentCreateViewModel
            {
                StartDateTime = System.DateTime.Now,
                EndDateTime = System.DateTime.Now,
                Activity = activityService.GetAll()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AppointmentCreateViewModel model)
        {
            //  var item = Mapper.Map<AppointmentViewModel, Appointment>(model);

            if (ModelState.IsValid)
            {
                Appointment appointment = new Appointment();

                appointment.StartDateTime = model.StartDateTime;
                appointment.EndDateTime = model.EndDateTime;
                appointment.UserId = Contex.currentUser.Id;
                appointment.ActivityId = model.ActivityId;

                service.Insert(appointment);

                return RedirectToAction("Index", "Appointment");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Appointment appointment = service.Get(id);

            service.Delete(appointment);

            return RedirectToAction("Index", "Appointment");
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            // var items = AutoMapper.Mapper.Map<Appointment, AppointmentEditViewModel>(service.Get(id));

            Appointment appointment = new Appointment();

            appointment = service.Get(id);

            AppointmentEditViewModel appointmentEditViewModel = new AppointmentEditViewModel
            {
                ActivityId = appointment.ActivityId,
                StartDateTime = appointment.StartDateTime,
                EndDateTime = appointment.EndDateTime,
                Activity = activityService.GetAll(),
            };

            return View(appointmentEditViewModel);
        }

        [HttpPost]
        public IActionResult Edit(AppointmentEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                // var item = Mapper.Map<AppointmentListViewModel, Appointment>(model);

                Appointment appointment = new Appointment();

                appointment.Id = model.Id;
                appointment.ActivityId = model.ActivityId;
                appointment.StartDateTime = model.StartDateTime;
                appointment.EndDateTime = model.EndDateTime;
                appointment.UserId = Contex.currentUser.Id;

                service.Update(appointment);

                return RedirectToAction("Index", "Appointment");
            }

            return View(model);
        }
    }
}