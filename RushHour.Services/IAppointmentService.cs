﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RushHour.Entities;

namespace RushHour.Services
{
    public interface IAppointmentService : IDataService<Appointment>
    {
        List<Appointment> GetAppointments();
    }
}