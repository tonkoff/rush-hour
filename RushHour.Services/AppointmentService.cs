﻿using System;
using System.Collections.Generic;
using System.Text;
using RushHour.Entities;
using RushHour.Data.Repositories;
using System.Linq;

namespace RushHour.Services
{
    public class AppointmentService : BaseDataService<Appointment>, IAppointmentService
    {
        private IAppointmentRepository appointmentRepository;

        public AppointmentService(IAppointmentRepository repository) : base(repository)
        {
            this.appointmentRepository = repository;
        }

        public List<Appointment> GetAppointments()
        {
            var result = appointmentRepository.GetAppointments().ToList();

            return result;
        }
    }
}