﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Entities
{
    public class Appointment : BaseEntity
    {
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public Guid UserId { get; set; }

        public Guid ActivityId { get; set; }

        public virtual Activity Activity { get; set; }

        //  public virtual HashSet<Activity> Activities { get; set; }
        public virtual User User { get; set; }
    }
}