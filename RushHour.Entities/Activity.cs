﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Entities
{
    public class Activity : BaseEntity
    {
        public string Name { get; set; }
        public float Duration { get; set; }
        public decimal Price { get; set; }

        public virtual HashSet<Appointment> Appointments { get; set; }

        // public virtual Appointment Appointment { get; set; }

        // public Guid AppointmentId { get; set; }
    }
}