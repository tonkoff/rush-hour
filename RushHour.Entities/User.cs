﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public virtual HashSet<Appointment> Appointments { get; set; }
    }
}